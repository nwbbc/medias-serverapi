﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.Entity;



namespace Medias_ServerApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            IocConfig.RegisterDependencies();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<Data.Db, Migrations.Configuration>);
        }
    }
}
