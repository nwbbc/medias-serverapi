﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medias_ServerApi.Entities
{
    public class Service
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ServiceDateTime { get; set; }
        public string Description { get; set; }
        public int SpeakerId { get; set; }
        public int ServiceTypeId { get; set; }
        public string Comments { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }


        public virtual Speaker Speaker { get; set; }
        public virtual ServiceType ServiceType { get; set; }

        
    }
}