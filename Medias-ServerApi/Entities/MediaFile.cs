﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medias_ServerApi.Entities
{
    public class MediaFile
    {
        public int Id { get; set; }
        public string PublicUrl { get; set; } //make readonly?
        public string PhysicalFilePath { get; set; } //make readonly?
        public bool Exists { get; set; } 
        public int Length { get; set; }  //length in seconds
        public int MediaFormatId { get; set; }
        public string Comments { get; set; } //for private use, describe problems with file for example
        public int UploadedById { get; set; }
        public int UploadedAt { get; set; }

        public virtual MediaFormat MediaFormat { get; set; }


    }
}