﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medias_ServerApi.Entities
{
    /// <summary>
    /// Specifies the type of service...examples are: Preaching, Wedding, Concert, Graduation, etc.
    /// Can determine what label is used for printing discs, as well as what is allowed for radio or web publishing.
    /// </summary>
    public class ServiceType
    {
    }
}