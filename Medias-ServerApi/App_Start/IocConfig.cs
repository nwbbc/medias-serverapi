﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace Medias_ServerApi
{
    public class IocConfig
    {
       public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            //builder.RegisterType(GetType(Data.Db)).AsSelf();
            //builder.RegisterType(GetType(BreezeDbContext)).AsSelf();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}